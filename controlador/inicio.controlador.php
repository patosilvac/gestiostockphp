<?php

class InicioControlador
{

    public function login()
    {
        require_once "vista/inicio/login.php";
    }

    public function logout()
    {
        if (session_status() == 2) {
            session_unset();
            setcookie(session_name(), 0, 1, ini_get("session.cookie_path"));
            session_destroy();
        }

        $GLOBALS['login_error'] = "";
        require_once "vista/inicio/login.php";
    }

    public function autenticacion()
    {
        require_once "modelo/dto/sesion.php";

        $sss = new Sesion();
        $sss->setUsuario(strtolower($_POST['txtUser']));
        $sss->setContrasegna($_POST['txtPass']);

        if ($sss->getUsuario() == null) {
            $GLOBALS['login_error'] = "Debe ingresar un usuario";
        }

        if ($sss->getContrasegna() == null) {
            $GLOBALS['login_error'] = "Debe ingresar una contraseña";
        }

        if ($sss->getUsuario() == 'admin') {
            if ($sss->getContrasegna() == 'admin') {
                $GLOBALS['login_error'] = "";
                session_start();
                $_SESSION['user'] = $sss->getUsuario();
                $_SESSION['name'] = "Administrador de sistema";
                require_once "vista/home.php";
            } else {
                $GLOBALS['login_error'] = "Usuario o contraseña incorrecto";
                require_once "vista/inicio/login.php";
            }
        } else {

            error_log(print_r($sss, true));
            $usuario = UsuarioCrud::buscarNombreUsuario($sss->getUsuario());

            if (is_array($usuario) || is_object($usuario)) {

                error_log(print_r($usuario, true));

                if ($usuario['NOMBRE_USUARIO'] != $sss->getUsuario()) {
                    error_log("usuario incorrecto");

                    $GLOBALS['login_error'] = "Usuario o contraseña incorrecto";
                    require_once "vista/inicio/login.php";
                } else if ($usuario['CONTRASEGNA'] != $sss->getContrasegna()) {
                    error_log("Contraseña incorrecta");

                    $GLOBALS['login_error'] = "Usuario o contraseña incorrecto";
                    require_once "vista/inicio/login.php";
                } else {
                    error_log("Iniciando sesion");
                    $GLOBALS['login_error'] = "";
                    session_start();
                    $_SESSION['user'] = $sss->getUsuario();
                    $_SESSION['name'] = $usuario['NOMBRE_COMPLETO'];
                    require_once "vista/home.php";
                }
            } else {
                error_log("Usuario no existe");
                $GLOBALS['login_error'] = "Usuario no existe";
                require_once "vista/inicio/login.php";
            }
        }
    }

    public function registrar()
    {
        require_once "vista/inicio/registrar.php";
    }

    public function nuevoUsuario()
    {
        if (empty($_POST['nombre'])) {
            Mensaje::alert("error", "Debe ingresar el nombre completo", "vista/inicio/registrar.php");
        } else if (empty($_POST['usuario'])) {
            Mensaje::alert("error", "Debe ingresar el nombre de usuario", "vista/inicio/registrar.php");
        } else if (empty($_POST['email'])) {
            Mensaje::alert("error", "Debe ingresar el e-mail", "vista/inicio/registrar.php");
        } else if (empty($_POST['password'])) {
            Mensaje::alert("error", "La contraseña no puede estar vacia", "vista/inicio/registrar.php");
        } else if (empty($_POST['repassword'])) {
            Mensaje::alert("error", "Debe ingresar la confirmación de la contraseña ", "vista/inicio/registrar.php");
        } else if ($_POST['password'] != $_POST['repassword']) {
            Mensaje::alert("error", "Las contraseñas deben ser iguales", "vista/inicio/registrar.php");
        } else {
            $usuario = new Usuario();
            $usuario->setNombre($_POST['nombre']);
            $usuario->setEmail($_POST['email']);
            $usuario->setNombreUsuario(strtolower($_POST['usuario']));
            $usuario->setContrasegna($_POST['password']);
            try {
                UsuarioCrud::insertar($usuario);
            } catch (Exception $e) {
                error_log($e->getCode() . " >> " . $e->getMessage());
                Mensaje::alert("error", "Error al guardar datos en BD (" . $e->getMessage() . ")", "vista/inicio/registrar.php");
            }

            Mensaje::alert("success", "Usuario guardado satisfactoriamente", "vista/inicio/login.php");
        }
    }
}
