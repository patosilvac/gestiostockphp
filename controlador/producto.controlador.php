<?php

class ProductoControlador
{

    public function home()
    {
        session_start();
        require_once "vista/home.php";
    }

    public function registro()
    {
        session_start();
        require_once "vista/producto/registro.php";
    }

    public function nuevo()
    {

        //Validacion php
        if (empty($_POST['codigo'])) {
            Mensaje::alert("error", "Debe ingresar el Codigo de Producto", "vista/producto/registro.php");
        } else if (empty($_POST['nombre'])) {
            Mensaje::alert("error", "Debe ingresar el Nombre de Producto", "vista/producto/registro.php");
        } else if (empty($_POST['descripcion'])) {
            Mensaje::alert("error", "Debe ingresar el Descripcion de Producto", "vista/producto/registro.php");
        } else if ($_POST['idCategoria'] == 0) {
            Mensaje::alert("error", "Debe ingresar el Categoria de Producto", "vista/producto/registro.php");
        } else if ($_POST['idEstado'] == 0) {
            Mensaje::alert("error", "Debe ingresar el Estado de Producto", "vista/producto/registro.php");
        } else {

            $prd = new Producto();
            $prd->setCodigo($_POST['codigo']);
            $prd->setNombre($_POST['nombre']);
            $prd->setDescripcion($_POST['descripcion']);
            $prd->setIdCategoria($_POST['idCategoria']);
            $prd->setIdEstado($_POST['idEstado']);

            error_log("Registrando producto");

            try {
                ProductoCrud::inserta($prd);
            } catch (Exception $e) {
                error_log($e->getCode() . " >> " . $e->getMessage());
                Mensaje::alert("error", "Error al guardar datos en BD (" . $e->getMessage() . ")", "vista/producto/registro.php");
            }

            Mensaje::alert("success", "Producto guardado satisfactoriamente", "vista/home.php");
        }
    }

    public function buscar()
    {
        session_start();
        $productos = null;
        require_once "vista/producto/buscar.php";
    }

    public function buscarResultado()
    {

        //validaciones
        if (empty($_POST['buscar'])) {
            Mensaje::alert("error", "Debe ingresar el Codigo o Nombre a buscar", "vista/producto/buscar.php");
        } else {
            $codigo = $_POST['buscar'];
            $tipoBusqueda = $_POST['buscarPor'];
            $sucursal = $_POST['sucursal'];

            if ($tipoBusqueda == 1) {
                if ($sucursal == 0) {
                    $productos = ProductoFinder::buscarPorCodigo($codigo);
                } else {
                    $productos = ProductoFinder::buscarPorCodigoSucursal($codigo, $sucursal);
                }
            } elseif ($tipoBusqueda == 2) {
                if ($sucursal == 0) {
                    $productos = ProductoFinder::buscarPorNombre($codigo);
                } else {
                    $productos = ProductoFinder::buscarPorNombreSucursal($codigo, $sucursal);
                }
            } else {
                Mensaje::alert("error", "Debe seleccionar por que atributo vamos a buscar Código/Nombre", "vista/producto/buscar.php");
            }
        }

        require_once "vista/producto/buscar.php";
    }

    public function borrar()
    {
        session_start();
        $productos = null;
        require_once "vista/producto/borrar.php";
    }

    public function buscarParaBorrar()
    {
        $codigo = $_POST['buscarBorrar'];
        $sucursal = $_POST['sucursal'];
        //validaciones
        if (empty($codigo) && $sucursal == 0) {
            Mensaje::alert("error", "Debe ingresar el Codigo o seleccionar Sucursal para borrar", "vista/producto/borrar.php");
        } else {
            if ($sucursal == 0) {
                $productos = ProductoFinder::buscarPorCodigo($codigo);
            } else {
                if (empty($codigo)) {
                    $productos = ProductoFinder::buscarPorSucursal($sucursal);
                } else {
                    $productos = ProductoFinder::buscarPorCodigoSucursal($codigo, $sucursal);
                }
            }
        }

        require_once "vista/producto/borrar.php";
    }

    public function editar()
    {
        $codigo = $_GET['cod'];
        $id_sucursal = $_GET['suc'];

        error_log("Buscando para editar codigo=" . $codigo . " sucursal=" . $id_sucursal);

        if ($id_sucursal == 0 || is_null($id_sucursal)) {
            $producto = ProductoFinder::buscarPorUnCodigo($codigo);
        } else {
            $producto = ProductoFinder::buscarPorUnCodigoSucursal($codigo, $id_sucursal);
        }

        require_once "vista/producto/editar.php";
    }

    public function actualizar()
    {

        //Validacion php
        if (empty($_POST['codigo'])) {
            Mensaje::alert("error", "Debe ingresar el Codigo de Producto", "vista//home.php");
        } else if (empty($_POST['nombre'])) {
            Mensaje::alert("error", "Debe ingresar el Nombre de Producto", "vista/home.php");
        } else if (empty($_POST['descripcion'])) {
            Mensaje::alert("error", "Debe ingresar el Descripcion de Producto", "vista/home.php");
        } else if (empty($_POST['cantidad'])) {
            Mensaje::alert("error", "Debe ingresar el Cantidad de Producto", "vista/home.php");
        } else if (empty($_POST['precio'])) {
            Mensaje::alert("error", "Debe ingresar el Precio de Producto", "vista/home.php");
        } else if ($_POST['idCategoria'] == '0') {
            Mensaje::alert("error", "Debe ingresar el Categoria de Producto", "vista/home.php");
        } else if ($_POST['idSucursal'] == '0') {
            Mensaje::alert("error", "Debe ingresar el Sucursal de Producto", "vista/home.php");
        } else if ($_POST['idEstado'] == '0') {
            Mensaje::alert("error", "Debe ingresar el Estado de Producto", "vista/home.php");
        } else {
            $prd = new Producto();
            $prd->setId($_GET['id']);
            $prd->setCodigo($_POST['codigo']);
            $prd->setNombre($_POST['nombre']);
            $prd->setDescripcion($_POST['descripcion']);
            $prd->setIdCategoria($_POST['idCategoria']);
            $prd->setIdEstado($_POST['idEstado']);

            $rel = new ProductoSucursal();

            $rel->setId_producto($_GET['id']);
            $rel->setId_sucursal($_POST['idSucursal']);
            $rel->setCantidad($_POST['cantidad']);
            $rel->setPrecio($_POST['precio']);

            error_log("producto.controlador.actualizar");

            try {
                ProductoSucursalCrud::insertar($rel);
            } catch (PDOException $e) {
                if ($e->getCode() == 23000 && strpos($e->getMessage(), "PROD_SUC_UN") == 0) {
                    error_log($e->getCode() . " >> " . $e->getMessage());
                    ProductoSucursalCrud::modifica($rel);
                } else {
                    error_log($e->getCode() . " >> " . $e->getMessage());
                    Mensaje::alert("error", "Error al guardar datos en BD ProductoSucursal (101)",  "vista/home.php");
                }
            }

            try {
                ProductoCrud::modifica($prd);
            } catch (PDOException $e) {
                error_log($e->getCode() . " >> " . $e->getMessage());
                Mensaje::alert("error", "Error al guardar datos en BD Producto (102)",  "vista/home.php");
            }

            Mensaje::alert("success", "Producto guardado satisfactoriamente", "vista/home.php");
            //require_once "vista/home.php";
        }
    }

    public function borrarProducto()
    {
        $id_producto = $_GET['cod'];

        error_log("Buscando para borrar codigo=" . $id_producto);

        if ($id_producto == 0 || is_null($id_producto)) {
            Mensaje::alert("error", "Error al Borrar datos en BD",  "vista/home.php");
        } else {
            $producto = ProductoCrud::borrar($id_producto);
            if ($producto > 0) {
                Mensaje::alert("success", "Producto marcado como inactivo satisfactoriamente",  "vista/home.php");
            } else {
                Mensaje::alert("error", "Hubo problemas para cambiar estado del producto",  "vista/home.php");
            }
        }

        require_once "vista/home.php";
    }
}
