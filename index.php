<?php

require_once 'utils/mensaje.php';
require_once 'modelo/conexion.php';
require_once 'modelo/repo/productoCrud.php';
require_once 'modelo/repo/productoFinder.php';
require_once 'modelo/repo/categoriaCrud.php';
require_once 'modelo/repo/sucursalCrud.php';
require_once 'modelo/repo/productoSucursalCrud.php';
require_once 'modelo/repo/estadoCrud.php';
require_once 'modelo/repo/usuarioCrud.php';
require_once 'modelo/dto/producto.php';
require_once 'modelo/dto/usuario.php';
require_once 'modelo/dto/productosucursal.php';

$GLOBALS['alerttitle'] = "titulo de alerta";
$GLOBALS['alertmessage'] = "mensaje de alerta";
$GLOBALS['login_error'] = "";

// https://anexsoft.com/realizando-un-crud-con-el-patron-mvc-en-php

function phpAlert($msg)
{
    echo '<script type="text/javascript">alert("' . $msg . '")</script>';
}


if (!isset($_GET['path'])) {
    // http://localhost:8080/gestion-stock/
    require_once "controlador/inicio.controlador.php";
    $controlador = new InicioControlador();
    call_user_func(array($controlador, "login"));
} else {
    // Caso de uso para SPA
    // http://localhost:8080/gestion-stock/?path=producto&accion=registro
    $controlador = $_GET['path'];
    require_once "controlador/$controlador.controlador.php";
    $controlador = ucwords($controlador) . "Controlador";
    $controlador = new $controlador;
    $metodo = isset($_GET['accion']) ? $_GET['accion'] : "inicio";
    call_user_func(array($controlador, $metodo));
}
