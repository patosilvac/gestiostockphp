<?php

class conexion {

    protected static $connection = null;

    public static function conecta() {       
        $options = [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
        ];
        try {
            if (isset($connection)){
                return $connection;
            }
            $connection = new PDO("mysql:host=localhost;dbname=gestionstock_php;charset=utf8mb4", "root", "", $options);
        } catch (Exception $e) {
            error_log($e->getMessage());
            exit(); //something a user can understand
        }
        return $connection;
    }        
}