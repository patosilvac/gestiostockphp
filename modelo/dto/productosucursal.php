<?php

class ProductoSucursal{

    private $id_producto;
    private $id_sucursal;
    private $cantidad;
    private $precio;

    /**
     * Get the value of id_producto
     */ 
    public function getId_producto()
    {
        return $this->id_producto;
    }

    /**
     * Set the value of id_producto
     *
     * @return  self
     */ 
    public function setId_producto($id_producto)
    {
        $this->id_producto = $id_producto;

        return $this;
    }

    /**
     * Get the value of id_sucursal
     */ 
    public function getId_sucursal()
    {
        return $this->id_sucursal;
    }

    /**
     * Set the value of id_sucursal
     *
     * @return  self
     */ 
    public function setId_sucursal($id_sucursal)
    {
        $this->id_sucursal = $id_sucursal;

        return $this;
    }

    /**
     * Get the value of cantidad
     */ 
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set the value of cantidad
     *
     * @return  self
     */ 
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get the value of precio
     */ 
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set the value of precio
     *
     * @return  self
     */ 
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }
}