<?php

class Sesion{

    private $usuario;
    private $contrasegna;

    /**
     * Get the value of usuario
     */ 
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set the value of usuario
     *
     * @return  self
     */ 
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get the value of contrasegna
     */ 
    public function getContrasegna()
    {
        return $this->contrasegna;
    }

    /**
     * Set the value of contrasegna
     *
     * @return  self
     */ 
    public function setContrasegna($contrasegna)
    {
        $this->contrasegna = $contrasegna;

        return $this;
    }

}