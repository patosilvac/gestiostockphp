<?php

class CategoriaCrud{

    public static function insertar(Categoria $categoria){
        $query = "insert into CATEGORIAS(NOMBRE_CAT) values(:nombre)";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombre', $categoria->getNombre(), PDO::PARAM_STR);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function modifica(Categoria $categoria){
        $query = "update CATEGORIAS set NOMBRE_CAT = :nombre where ID_CATEGORIA = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombre', $categoria->getNombre(), PDO::PARAM_STR);
        $stt->bindValue(':id', $categoria->getId(), PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function borrar($id_categoria){
        $query = "delete from CATEGORIAS where ID_CATEGORIA = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id', $id_categoria, PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function buscar($id_categoria){
        $query = "select * from CATEGORIAS where ID_CATEGORIA = ?";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id', $id_categoria, PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function buscarTodo(){
        $query = "select * from CATEGORIAS";
        return conexion::conecta()->query($query);
    }



}