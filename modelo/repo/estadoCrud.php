<?php

class EstadoCrud{

    public static function insertar(Estado $estado){
        $query = "insert into ESTADO(NOMBRE_EST) values(:nombre)";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombre', $estado->getNombre(), PDO::PARAM_STR);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function modifica(Estado $estado){
        $query = "update ESTADO set NOMBRE_EST = :nombre where ID_ESTADO = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombre', $estado->getNombre(), PDO::PARAM_STR);
        $stt->bindValue(':id', $estado->getId(), PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function borrar($id_estado){
        $query = "delete from ESTADO where ID_ESTADO = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id', $id_estado, PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function buscar($id_estado){
        $query = "select * from ESTADO where ID_ESTADO = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id', $id_estado, PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function buscarTodo(){
        $query = "select * from ESTADO";
        return conexion::conecta()->query($query);
    }

}