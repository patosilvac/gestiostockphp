<?php

class ProductoCrud
{
    public static function inserta(Producto $producto)
    {
        $query = "insert into PRODUCTOS(CODIGO, NOMBRE_PROD, DESC_PROD, ID_CATEGORIA, ID_ESTADO) 
                    values (:codigo,:nombre,:descripcion,:id_categoria,:id_estado)";

        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':codigo', $producto->getCodigo(), PDO::PARAM_INT);
        $stt->bindValue(':nombre', $producto->getNombre(), PDO::PARAM_STR);
        $stt->bindValue(':descripcion', $producto->getDescripcion(), PDO::PARAM_STR);
        $stt->bindValue(':id_categoria', $producto->getIdCategoria(), PDO::PARAM_INT);
        $stt->bindValue(':id_estado', $producto->getIdEstado(), PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function modifica(Producto $producto)
    {
        $query = "update PRODUCTOS set CODIGO=:codigo, NOMBRE_PROD=:nombre, DESC_PROD=:descripcion, ID_CATEGORIA=:id_categoria, ID_ESTADO=:id_estado WHERE ID_PRODUCTO=:id_producto";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':codigo', $producto->getCodigo(), PDO::PARAM_STR);
        $stt->bindValue(':nombre', $producto->getNombre(), PDO::PARAM_STR);
        $stt->bindValue(':descripcion', $producto->getDescripcion(), PDO::PARAM_STR);
        $stt->bindValue(':id_categoria', $producto->getIdCategoria(), PDO::PARAM_INT);
        $stt->bindValue(':id_estado', $producto->getIdEstado(), PDO::PARAM_INT);
        $stt->bindValue(':id_producto', $producto->getId(), PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function borrar($codigo)
    {
        $query = "update PRODUCTOS set id_estado=2 where CODIGO = :codigo";
        error_log($query);
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':codigo', $codigo, PDO::PARAM_STR);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function todos()
    {
        $query = "select * from PRODUCTOS";
        return conexion::conecta()->query($query);
    }
}
