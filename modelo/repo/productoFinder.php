<?php

class ProductoFinder{

    const basequery = "select p.*, ps.CANTIDAD, ps.PRECIO, c.NOMBRE_CAT,  s.NOMBRE_SUC, e.NOMBRE_EST , s.ID_SUCURSAL 
                                from PRODUCTOS as p 
                                    left join PROD_SUC as ps 
                                    on ps.ID_PRODUCTO=p.ID_PRODUCTO 
                                    left join SUCURSALES as s
                                    on s.ID_SUCURSAL = ps.ID_SUCURSAL
                                    left join CATEGORIAS c
                                    on c.ID_CATEGORIA = p.ID_CATEGORIA
                                    left join ESTADO e
                                    on e.ID_ESTADO = p.ID_ESTADO
                                where 1=1 ";

 
    public static function buscarPorUnCodigo($codigo){
        $query = ProductoFinder::basequery." and p.CODIGO = :codigoProducto ";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':codigoProducto', $codigo, PDO::PARAM_STR);
        $stt->execute();
        return $stt->fetch();
    }

    public static function buscarPorUnCodigoSucursal($codigo, $id_sucursal){
        $query = ProductoFinder::basequery."and p.CODIGO = :codigoProducto and ps.ID_SUCURSAL = :id_sucursal";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':codigoProducto', $codigo, PDO::PARAM_STR);
        $stt->bindValue(':id_sucursal', $id_sucursal, PDO::PARAM_INT);
        $stt->execute();
        return $stt->fetch();
    }

    public static function buscarPorCodigo($codigo){
        $query = ProductoFinder::basequery."and p.CODIGO = :codigoProducto ";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':codigoProducto', $codigo, PDO::PARAM_STR);
        $stt->execute();
        return $stt->fetchAll();
    }

    public static function buscarPorSucursal($sucursal){
        $query = ProductoFinder::basequery."and ps.ID_SUCURSAL = :sucursal ";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':sucursal', $sucursal, PDO::PARAM_INT);
        $stt->execute();
        return $stt->fetchAll();
    }

    public static function buscarPorNombre($nombre_producto){
        $keyword = "%".$nombre_producto."%";
        $query = ProductoFinder::basequery."and p.NOMBRE_PROD like :nombreProducto ";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombreProducto', $keyword, PDO::PARAM_STR);
        $stt->execute();
        return $stt->fetchAll();
    }

    public static function buscarPorCodigoSucursal($codigo, $id_sucursal){
        $query = ProductoFinder::basequery."and p.CODIGO = :codigoProducto and ps.ID_SUCURSAL = :sucursal ";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':codigoProducto', $codigo, PDO::PARAM_STR);
        $stt->bindValue(':sucursal', $id_sucursal, PDO::PARAM_INT);
        $stt->execute();
        return $stt->fetchAll();
    }

    public static function buscarPorNombreSucursal($nombre_producto, $id_sucursal){
        $keyword = "%".$nombre_producto."%";
        $query = ProductoFinder::basequery."and p.NOMBRE_PROD like :nombreProducto and ps.ID_SUCURSAL = :sucursal ";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombreProducto', $keyword, PDO::PARAM_STR);
        $stt->bindValue(':sucursal', $id_sucursal, PDO::PARAM_INT);
        $stt->execute();
        return $stt->fetchAll();
    }

    public static function buscarVacio(){
        $query = ProductoFinder::basequery." and 1=2 ";
        return conexion::conecta()->query($query);
    }

    public static function buscarTodo(){
        $query = ProductoFinder::basequery . "and p.ID_ESTADO=1";
        return conexion::conecta()->query($query);
    }
        
}