<?php

class ProductoSucursalCrud{

    public static function insertar(ProductoSucursal $prodsuc){
        $query = "insert into PROD_SUC(ID_PRODUCTO, ID_SUCURSAL, CANTIDAD, PRECIO) 
                    values(:id_producto, :id_sucursal, :cantidad, :precio)";
        
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':cantidad', $prodsuc->getCantidad(), PDO::PARAM_INT);
        $stt->bindValue(':precio', $prodsuc->getPrecio(), PDO::PARAM_INT);
        $stt->bindValue(':id_producto', $prodsuc->getId_producto(), PDO::PARAM_INT);
        $stt->bindValue(':id_sucursal', $prodsuc->getId_sucursal(), PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function modifica(ProductoSucursal $prodsuc){
        $query = "update PROD_SUC set CANTIDAD = :cantidad, PRECIO = :precio where ID_PRODUCTO = :id_producto and ID_SUCURSAL = :id_sucursal";

        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':cantidad', $prodsuc->getCantidad(), PDO::PARAM_INT);
        $stt->bindValue(':precio', $prodsuc->getPrecio(), PDO::PARAM_INT);
        $stt->bindValue(':id_producto', $prodsuc->getId_producto(), PDO::PARAM_INT);
        $stt->bindValue(':id_sucursal', $prodsuc->getId_sucursal(), PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function borrar($id_producto, $id_sucursal){
        $query = "delete from PROD_SUC where ID_PRODUCTO = :id_producto and ID_SUCURSAL = :id_sucursal";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id_producto', $id_producto, PDO::PARAM_INT);
        $stt->bindValue(':id_sucursal', $id_sucursal, PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function buscar($id_producto, $id_sucursal){
        $query = "select * from PROD_SUC where ID_PRODUCTO = :id_producto and ID_SUCURSAL = :id_sucursal";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id_producto', $id_producto, PDO::PARAM_STR);
        $stt->bindValue(':id_sucursal', $id_sucursal, PDO::PARAM_STR);
        $stt->execute();
        return $stt->fetch();
    }

    public static function buscarTodo(){
        $query = "select * from PROD_SUC";
        return conexion::conecta()->query($query);
    }

}