<?php

class SucursalCrud{

    public static function insertar(Sucursal $sucursal){
        $query = "insert into SUCURSALES(NOMBRE_SUC) values(:nombre)";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombre', $sucursal->getNombre(), PDO::PARAM_STR);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function modifica(Sucursal $sucursal){
        $query = "update SUCURSALES set NOMBRE_SUC = :nombre where ID_SUCURSAL = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombre', $sucursal->getNombre(), PDO::PARAM_STR);
        $stt->bindValue(':id', $sucursal->getId(), PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function borrar($id_sucursal){
        $query = "delete from SUCURSALES where ID_SUCURSAL = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id', $id_sucursal, PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function buscar($id_sucursal){
        $query = "select * from SUCURSALES where ID_SUCURSAL = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id', $id_sucursal, PDO::PARAM_INT);
        $stt->execute();
        return $stt->fetchAll();
    }

    public static function buscarUno($id_sucursal){
        $query = "select * from SUCURSALES where ID_SUCURSAL = :id_sucursal ";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id_sucursal', $id_sucursal, PDO::PARAM_INT);
        $stt->execute();
        return $stt->fetch();
    }

    public static function buscarTodo(){
        $query = "select * from SUCURSALES";
        return conexion::conecta()->query($query);
    }

}