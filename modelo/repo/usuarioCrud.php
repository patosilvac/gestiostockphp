<?php

class UsuarioCrud
{

    public static function insertar(Usuario $usuario)
    {
        $query = "insert into USUARIOS(NOMBRE_COMPLETO, NOMBRE_USUARIO, CORREO, CONTRASEGNA) values(:nombre, :nombreUsuario, :email, :contrasegna)";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombre', $usuario->getNombre(), PDO::PARAM_STR);
        $stt->bindValue(':nombreUsuario', $usuario->getNombreUsuario(), PDO::PARAM_STR);
        $stt->bindValue(':email', $usuario->getEmail(), PDO::PARAM_STR);
        $stt->bindValue(':contrasegna', $usuario->getContrasegna(), PDO::PARAM_STR);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function modifica(Usuario $usuario)
    {
        $query = "update USUARIOS set NOMBRE_USUARIO = :nombreUsuario, NOMBRE_COMPLETO = :nombre, CORREO = :email, CONTRASEGNA = :contrasegna  where ID_USUARIO = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombre', $usuario->getNombre(), PDO::PARAM_STR);
        $stt->bindValue(':nombreUsuario', $usuario->getNombreUsuario(), PDO::PARAM_STR);
        $stt->bindValue(':email', $usuario->getEmail(), PDO::PARAM_STR);
        $stt->bindValue(':contrasegna', $usuario->getContrasegna(), PDO::PARAM_STR);
        $stt->bindValue(':id', $usuario->getId(), PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function borrar($id_usuario)
    {
        $query = "delete from USUARIOS where ID_USUARIO = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id', $id_usuario, PDO::PARAM_INT);
        $stt->execute();
        return $stt->rowCount();
    }

    public static function buscar($id_usuario)
    {
        $query = "select * from USUARIOS where ID_USUARIO = :id";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id', $id_usuario, PDO::PARAM_INT);
        $stt->execute();
        return $stt->fetchAll();
    }

    public static function buscarUno($id_usuario)
    {
        $query = "select * from USUARIOS where ID_USUARIO = :id_usuario ";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $stt->execute();
        return $stt->fetch();
    }

    public static function buscarNombreUsuario($nombreUsuario)
    {
        $query = "select * from USUARIOS where NOMBRE_USUARIO = :nombreUsuario ";
        $stt = conexion::conecta()->prepare($query);
        $stt->bindValue(':nombreUsuario', $nombreUsuario, PDO::PARAM_STR);
        $stt->execute();
        return $stt->fetch();
    }

    public static function buscarTodo()
    {
        $query = "select * from USUARIOS";
        return conexion::conecta()->query($query);
    }
}
