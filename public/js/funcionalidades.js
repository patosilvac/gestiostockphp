$(document).ready(function() {
    $("#individual").click(function() {
        $("#div1").hide();
        $("#div2").show();

    });

    $("#multiples").click(function() {
        $("#div1").show();
        $("#div2").hide();

    });
});


function validaRegistro() {

    //campos de formulario agregar producto
    var codigo = document.getElementById("codigo").value;
    var nombre = document.getElementById("nombre").value;
    var descripcion = document.getElementById("descripcion").value;
    var categoria = document.getElementById("idCategoria").selectedIndex;
    var estado = document.getElementById("idEstado").selectedIndex;

    //campo cantidad y precio de formulario Editar producto
    var cantidad = document.getElementById("cantidad").value;
    var precio = document.getElementById("precio").value;
    var sucursal = document.getElementById("idSucursal").selectedIndex;

    // campo buscar y opciones de consultar producto
    var buscar = document.getElementById("buscar").value;
    var opciones = document.getElementsByName("buscarPor");

    // campo de busqueda en borrar producto
    var buscarBorrar = document.getElementById("buscarBorrar").value;

    // Validaciones
    if (codigo == null || codigo == 0) {
        alert("Indicar un valor en el campo CODIGO");
        document.getElementById("codigo").focus();
        return false;
    }

    if (nombre == null || nombre.length == 0 || /^\s+$/.test(nombre)) {
        alert("Indicar un valor en el campo NOMBRE");
        document.getElementById("nombre").focus();
        return false;
    }

    if (descripcion == null || descripcion.length == 0 || /^\s+$/.test(descripcion)) {
        alert("Indicar un valor en el campo DESCRIPCION");
        document.getElementById("descripcion").focus();
        return false;
    }

    if (cantidad == null || catindad == 0) {
        alert("Indicar un valor en el campo CANTIDAD");
        document.getElementById("cantidad").focus();
    }

    if (precio == null || precio == 0) {
        alert("Indicar un valor en el campo PRECIO");
        document.getElementById("precio").focus();
    }

    if (categoria == null || categoria == 0) {
        alert("Indicar un valor en el campo CATEGORIA");
        document.getElementById("idCategoria").focus();
        return false;
    }

    if (sucursal == null || sucursal == 0) {
        alert("Indicar un valor en el campo SUCURSAL");
        document.getElementById("idCategoria").focus();
        return false;
    }

    if (estado == null || estado == 0) {
        alert("Indicar un valor en el campo ESTADO");
        document.getElementById("idEstado").focus();
        return false;
    }

    // input busqueda consulta producto
    if (buscar == null || buscar.length == 0) {
        alert("Indicar un valor en el campo Buscar");
        document.getElementById("buscar").focus();
        return false;
    }

    //Validar opciones de consultar producto
    var seleccionado = false;
    for (var i = 0; i < opciones.length; i++) {
        if (opciones[i].checked) {
            seleccionado = true;
            break;
        }
    }

    if (!seleccionado) {
        return false;
    }

    // input busqueda borrar producto
    if (buscarBorrar == null || buscarBorrar.length == 0) {
        alert("Indicar un valor en el campo Buscar");
        document.getElementById("buscarBorrar").focus();
        return false;
    }

}