function validaCrearProducto() {

    //campos de formulario agregar producto
    //alert("prueba")

    cod = document.getElementById("codigo").value;
    nom = document.getElementById("nombre").value;
    des = document.getElementById("descripcion").value;
    cat = document.getElementById("idCategoria").selectedIndex;
    est = document.getElementById("idEstado").selectedIndex;

    if (cod == null || cod == "") {
        alert("Debe llenar el campo codigo")
        document.getElementById("codigo").focus;
        return false;
    } else if (nom == null || nom.length == 0 || /^\s+$/.test(nom)) {
        alert("Debe llenar el campo nombre")
        document.getElementById("nombre").focus;
        return false;
    } else if (des == null || des.length == 0 || /^\s+$/.test(des)) {
        alert("Debe llenar el campo descripcion")
        document.getElementById("descripcion").focus;
        return false;
    } else if (cat == null || cat == 0) {
        alert("Indicar un valor en el campo CATEGORIA");
        document.getElementById("idCategoria").focus();
        return false;
    } else if (est == null || est == 0) {
        alert("Indicar un valor en el campo ESTADO");
        document.getElementById("idEstado").focus();
        return false;
    } else {
        return true;
    }

}


function validaEditarProducto() {

    //campos de formulario agregar producto
    //alert("prueba")

    var cod = document.getElementById("codigo").value;
    var nom = document.getElementById("nombre").value;
    var des = document.getElementById("descripcion").value;
    var can = document.getElementById("cantidad").value;
    var pre = document.getElementById("precio").value;
    var cat = document.getElementById("idCategoria").selectedIndex;
    var suc = document.getElementById("idSucursal").selectedIndex;
    var est = document.getElementById("idEstado").selectedIndex;


    if (cod == null || cod == "") {
        alert("Debe llenar el campo codigo")
        document.getElementById("codigo").focus;
        return false;
    } else if (nom == null || nom.length == 0 || /^\s+$/.test(nom)) {
        alert("Debe llenar el campo nombre")
        document.getElementById("nombre").focus;
        return false;
    } else if (des == null || des.length == 0 || /^\s+$/.test(des)) {
        alert("Debe llenar el campo descripcion")
        document.getElementById("descripcion").focus;
        return false;
    } else if (can == null || can == 0) {
        alert("Indicar un valor en el campo CANTIDAD");
        document.getElementById("cantidad").focus();
        return false;
    } else if (pre == null || pre == 0) {
        alert("Indicar un valor en el campo PRECIO");
        document.getElementById("precio").focus();
        return false;
    } else if (cat == null || cat == 0) {
        alert("Indicar un valor en el campo CATEGORIA");
        document.getElementById("idCategoria").focus();
        return false;
    } else if (suc == null || suc == 0) {
        alert("Indicar un valor en el campo SUCURSAL");
        document.getElementById("idSucursal").focus();
        return false;
    } else if (est == null || est == 0) {
        alert("Indicar un valor en el campo ESTADO");
        document.getElementById("idEstado").focus();
        return false;
    } else {
        return true;
    }

}

function validaBuscar() {

    // campo buscar y opciones de consultar producto
    bus = document.getElementById("buscar").value;

    // input busqueda consulta producto
    if (bus == null || bus.length == 0 || bus == 0) {
        alert("Indicar un valor en el campo Buscar");
        document.getElementById("buscar").focus();
        return false;
    }
}

function validaBuscarBorrar() {

    // campo buscar y opciones de consultar producto
    busBorra = document.getElementById("buscarBorrar").value;
    suc = document.getElementById("sucursal").selectedIndex;

    if (suc > 0) {
        return true;
    } else {
        //input busqueda consulta producto
        if (busBorra == null || busBorra.length == 0 || busBorra == 0) {
            alert("Indicar un valor en el campo Codigo");
            document.getElementById("buscarBorrar").focus();
            return false;
        } else {
            return true;
        }
    }

}

function validaNuevoUsuario() {
    //alert("prueba")

    nom = document.getElementById("nombre").value;
    mail = document.getElementById("email").value;
    usu = document.getElementById("usuario").value;
    pass = document.getElementById("password").value;
    repass = document.getElementById("repassword").value;

    if (nom == null || nom.length == 0 || /^\s+$/.test(nom)) {
        alert("Debe llenar el campo nombre")
        document.getElementById("Nombre").focus;
        return false;
    } else if (mail == null || mail.length == 0) {
        alert("Debe llenar el campo email")
        document.getElementById("email").focus;
        return false;
    } else if (usu == null || usu.length == 0) {
        alert("Debe llenar el campo Usuario")
        document.getElementById("usuario").focus;
        return false;
    } else if (pass == null || pass.length == 0) {
        alert("Debe llenar el campo Contraseña")
        document.getElementById("password").focus;
        return false;
    } else if (repass == null || repass.length == 0) {
        alert("Debe llenar el campo Confirmar Contraseña")
        document.getElementById("repassword").focus;
        return false;
    } else if (repass != pass) {
        alert("contraseñas deben coincidir")
        document.getElementById("repassword").focus;
        return false;
    } else {
        return true;
    }

}

function ValidaLogin() {
    //alert("prueba de alert y js en login")

    user = document.getElementById('txtUser').value;
    pass = document.getElementById('txtPass').value;

    if (user == null || user.length == 0 || /^\s+$/.test(user)) {
        alert("Debe llenar el campo Usuario")
        document.getElementById("txtUser").focus;
        return false;
    } else if (pass == null || pass.length == 0) {
        alert("Debe llenar el campo Contraseña")
        document.getElementById("txtPass").focus;
        return false;
    } else {
        return true;
    }

}