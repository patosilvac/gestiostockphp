/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     05/10/2020 11:01:22                          */
/*==============================================================*/


alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_ESTA_ESTADO;

alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_PERTENECE_CATEGORI;

alter table PROD_SUC 
   drop foreign key FK_PROD_SUC_PROD_SUC_PRODUCTO;

alter table PROD_SUC 
   drop foreign key FK_PROD_SUC_PROD_SUC2_SUCURSAL;

drop table if exists CATEGORIAS;

drop table if exists ESTADO;


alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_PERTENECE_CATEGORI;

alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_ESTA_ESTADO;

drop table if exists PRODUCTOS;


alter table PROD_SUC 
   drop foreign key FK_PROD_SUC_PROD_SUC_PRODUCTO;

alter table PROD_SUC 
   drop foreign key FK_PROD_SUC_PROD_SUC2_SUCURSAL;

drop table if exists PROD_SUC;

drop table if exists SUCURSALES;

/*==============================================================*/
/* Table: CATEGORIAS                                            */
/*==============================================================*/
create table CATEGORIAS
(
   ID_CATEGORIA         int not null AUTO_INCREMENT comment '',
   NOMBRE_CAT           varchar(255) not null  comment '',
   primary key (ID_CATEGORIA)
);

/*==============================================================*/
/* Table: ESTADO                                                */
/*==============================================================*/
create table ESTADO
(
   ID_ESTADO            int not null AUTO_INCREMENT comment '',
   NOMBRE_EST           varchar(255) not null  comment '',
   primary key (ID_ESTADO)
);

/*==============================================================*/
/* Table: USUARIOS                                              */
/*==============================================================*/
CREATE TABLE USUARIOS (
	ID_USUARIO INT auto_increment NOT NULL,
	NOMBRE_COMPLETO varchar(200) NULL,
	NOMBRE_USUARIO varchar(50) NOT NULL,
	CORREO varchar(100) NULL,
	CONTRASEGNA varchar(100) NOT NULL,
	PRIMARY KEY (ID_USUARIO)
);

CREATE UNIQUE INDEX USUARIO_NOMBRE_IDX USING BTREE ON gestionstock_php.USUARIOS (NOMBRE_USUARIO);

ALTER TABLE gestionstock_php.USUARIOS ADD CONSTRAINT USUARIOS_UN UNIQUE KEY (NOMBRE_USUARIO);


/*==============================================================*/
/* Table: PRODUCTOS                                             */
/*==============================================================*/
create table PRODUCTOS
(
   ID_PRODUCTO          int not null AUTO_INCREMENT comment '',
   ID_ESTADO            int  comment '',
   ID_CATEGORIA         int not null  comment '',
   NOMBRE_PROD          varchar(100) not null  comment '',
   DESC_PROD            varchar(255) not null  comment '',
   CODIGO               varchar(20) not null comment '',
   primary key (ID_PRODUCTO)
);

CREATE UNIQUE INDEX PRODUCTOS_CODIGO_IDX USING BTREE ON gestionstock_php.PRODUCTOS (CODIGO);


/*==============================================================*/
/* Table: PROD_SUC                                              */
/*==============================================================*/
create table PROD_SUC
(
   ID_PRODUCTO          int not null  comment '',
   ID_SUCURSAL          int not null  comment '',
   CANTIDAD             int  comment '',
   PRECIO               int  comment '',
   primary key (ID_PRODUCTO, ID_SUCURSAL)
);

/*==============================================================*/
/* Table: SUCURSALES                                            */
/*==============================================================*/
create table SUCURSALES
(
   ID_SUCURSAL          int not null AUTO_INCREMENT comment '',
   NOMBRE_SUC           varchar(255) not null  comment '',
   primary key (ID_SUCURSAL)
);

alter table PRODUCTOS add constraint FK_PRODUCTO_ESTA_ESTADO foreign key (ID_ESTADO)
      references ESTADO (ID_ESTADO) on delete restrict on update restrict;

alter table PRODUCTOS add constraint FK_PRODUCTO_PERTENECE_CATEGORI foreign key (ID_CATEGORIA)
      references CATEGORIAS (ID_CATEGORIA) on delete restrict on update restrict;

alter table PROD_SUC add constraint FK_PROD_SUC_PROD_SUC_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTOS (ID_PRODUCTO) on delete restrict on update restrict;

alter table PROD_SUC add constraint FK_PROD_SUC_PROD_SUC2_SUCURSAL foreign key (ID_SUCURSAL)
      references SUCURSALES (ID_SUCURSAL) on delete restrict on update restrict;

