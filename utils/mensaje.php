<?php

class Mensaje
{

    public static function alert($status_code, $status, $uri)
    {
        error_log("Utils/Mensaje.alert " . $status_code . "-" . $status);
        $_SESSION['status_code'] = $status_code;
        $_SESSION['status'] = $status;

        require str_replace('/gestiostockphp/', '', $uri);
    }
}
