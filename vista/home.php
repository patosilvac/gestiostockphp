<?php require_once "vista/inc/header.php";  ?>

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-12">
                <div class="col-sm-12">
                    <h1>LISTADO DE PRODUCTOS</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <!-- row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body table-responsive p-0">
                            <?php 
                                $productos = ProductoFinder::buscarTodo();
                                //$productos = ProductoCrud::todos();
                            ?>
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                        <th>Descripcion</th>
                                        <th>Cantidad</th>
                                        <th>Precio</th>
                                        <th>Categoria</th>
                                        <th>Sucursal</th>
                                        <th>Estado</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (is_array($productos) || is_object($productos)){
                                        foreach ($productos as $row){
                                            echo '<tr>';
                                            echo '<td>'.$row['ID_PRODUCTO'].'</td>';
                                            echo '<td>'.$row['CODIGO'].'</td>';
                                            echo '<td>'.$row['NOMBRE_PROD'].'</td>';
                                            echo '<td>'.$row['DESC_PROD'].'</td>';
                                            echo '<td>'.$row['CANTIDAD'].'</td>';
                                            echo '<td>'.$row['PRECIO'].'</td>';
                                            echo '<td>'.$row['NOMBRE_CAT'].'</td>';
                                            echo '<td>'.$row['NOMBRE_SUC'].'</td>';
                                            echo '<td>'.$row['NOMBRE_EST'].'</td>';
                                            echo '<td><a class="btn btn-success" href="?path=producto&accion=editar&cod='.$row['CODIGO'].'&suc='.$row['ID_SUCURSAL'].'">editar</a></td>';
                                            echo '</tr>';
                                            
                                        }
                                    } 
                                    ?>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section> <!-- /.row -->
</div><!-- /.container-fluid -->

<!-- /.content -->
</div>

<?php require_once "vista/inc/footer.php";  ?>