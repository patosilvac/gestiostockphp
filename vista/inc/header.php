<?php require_once "vista/inc/head.php";  ?>

<body class="hold-transition sidebar-mini">

    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-primary navbar-dark sticky-top">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto float-right">
                <li class="nav-item text-nowrap px-md-5">
                    <a class="btn btn-light" href="?path=inicio&accion=logout">Cerrar
                        sesión</a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="?path=producto&accion=home" class="brand-link">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="card text-white bg-primary text-center" style="max-width: 15rem;">
                            <div class="card-body">
                                <h2 class="card-title">GS</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="imagen">
                        <i class="fa fa-user fa-3x text-white"></i>
                    </div>
                    <div class="info">
                        <p class="text-center text-white"><?php echo $_SESSION['user'] ?></p>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                        <!-- Area de menu Productos -->
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fab fa-product-hunt"></i>
                                <p>
                                    PRODUCTOS
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="?path=producto&accion=home" class="nav-link">
                                        <i class="nav-icon fas fa-list text-primary"></i>
                                        <p>Listar Productos</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?path=producto&accion=registro" class="nav-link">
                                        <i class="nav-icon fas fa-plus text-success"></i>
                                        <p>Agregar Producto</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?path=producto&accion=buscar" class="nav-link">
                                        <i class="nav-icon fas fa-search text-primary"></i>
                                        <p>Consultar producto</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?path=producto&accion=borrar" class="nav-link">
                                        <i class="nav-icon fas fa-minus text-danger"></i>
                                        <p>Borrar producto</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!--
                        // Area de menu Sucursales 
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p>
                                    SUCURSALES
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-plus text-success"></i>
                                        <p>Agregar Sucursal</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-search text-info"></i>
                                        <p>Consultar Sucursal</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        // Area de menu Categorias 
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-th"></i>
                                <p>
                                    CATEGORIAS
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-plus text-success"></i>
                                        <p>Agregar Categoria</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-search text-info"></i>
                                        <p>Consultar Categoria</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        // Area de menu Usuarios 
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    USUARIOS
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/view/usuarios/add.php" class="nav-link">
                                        <i class="nav-icon fas fa-user-plus text-success"></i>
                                        <p>Agregar Usuario</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/view/usuarios/show.php" class="nav-link">
                                        <i class="nav-icon fas fa-search text-info"></i>
                                        <p>Consultar Usuarios</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
-->
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>