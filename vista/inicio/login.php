<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login GS</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="public/css/signin.css">

    <!-- Validation JavaScript -->
    <script type="text/javascript" src="public/js/valida.js"></script>
</head>

<body class="text-center">
    <div>
        <form class="form-signin" onsubmit="return ValidaLogin()" method="post" action="?path=inicio&accion=autenticacion">
            <!-- logo -->
            <div class="container">
                <div class="row justify-content-center">
                    <div class="card text-white bg-primary mb-4 text-center" style="max-width: 15rem;">
                        <div class="card-body">
                            <h2 class="card-title">GS</h2>
                        </div>
                    </div>
                </div>
            </div>

            <h1 class="h4 mb-3 font-weight-normal">SISTEMA GESTION STOCK</h1>
            <div class="form-group row">
                <input type="text" name="txtUser" id="txtUser" class="form-control" placeholder="Usuario" required>
            </div>
            <div class="form-group row">
                <input type="password" name="txtPass" id="txtPass" class="form-control" placeholder="Contraseña" required>
            </div>
            <div class="checkbox mb-3">
                <div>
                    <!-- mensaje a definir -->
                    <span class="text-danger"><?php echo $GLOBALS['login_error'] ?> </span>
                </div>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit" value="Ingresar" name="btnLogin">Ingresar</button>
        </form>
        <div class="row justify-content-center">
            <a href="?path=inicio&accion=registrar">Registrar</a>
        </div>
        <p class="mt-5 mb-3 text-muted">&copy; RA_PS_CQ - CIISA</p>
    </div>

</body>

</html>