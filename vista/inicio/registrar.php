<?php
require_once "vista/inc/head.php";
?>

<body class="bg-light">
    <div class="container">
        <div class="row mb-12">
            <!-- logo -->
            <div class="container">
                <div class="row justify-content-center p-3">
                    <div class="card text-white bg-primary mb-4 text-center" style="max-width: 15rem;">
                        <div class="card-body">
                            <h2 class="card-title">GS</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row mb-12">
                    <div class="col-sm-12">
                        <h1 class="text-center p-3">REGISTRO DE USUARIO</h1>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="col-sm-12">
                    <div class="card p-3 mb-2 bg-white text-dark">
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-12">
                            <!-- formulario -->
                            <form class="form-horizontal" onsubmit="return validaNuevoUsuario()" method="POST" action="?path=inicio&accion=nuevoUsuario">
                                <div class="form-group row">
                                    <label for="nombre" class="col-sm-4 col-form-label">Nombre</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="nombre" id="nombre" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label">E-mail</label>
                                    <div class="col-sm-12">
                                        <input type="email" class="form-control" name="email" id="email" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="usuario" class="col-sm-4 col-form-label">Usuario</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="usuario" id="usuario" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-sm-4 col-form-label">Contraseña</label>
                                    <div class="col-sm-12">
                                        <input type="password" class="form-control" name="password" id="password" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="repassword" class="col-sm-4 col-form-label">Confirmar Contraseña</label>
                                    <div class="col-sm-12">
                                        <input type="password" class="form-control" name="repassword" id="repassword" required>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-center h-100">
                                    <div class="col-sm-10 align-self-center text-center">
                                        <button type="submit" class="btn btn-success">Registrar</button>
                                        <a href="?path=inicio&accion=login" class="btn btn-danger">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                            <!-- end formulario -->
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</body>




<?php require_once "vista/inc/footer.php";  ?>