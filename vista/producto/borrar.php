<?php
require_once "vista/inc/header.php";
?>

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-12">
                <div class="col-sm-12">
                    <h1>BORRAR O DAR DE BAJA UN PRODUCTO</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Cuadro de busqueda</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-12">
                            <?php
                            $sucursales = SucursalCrud::buscarTodo();
                            ?>
                            <!-- Inicio Formulario buscar -->
                            <form class="form-horizontal" onsubmit="return validaBuscarBorrar()" method="POST" action="?path=producto&accion=buscarParaBorrar">
                                <div class="form-row">
                                    <div class="form-group col-md-9">
                                        <label>Ingrese Codigo</label>
                                        <input type="text" class="form-control" name="buscarBorrar" id="buscarBorrar">
                                    </div>
                                </div>
                                <p>¿Deseas buscar por Sucursal?
                                    <a class="" data-toggle="collapse" href="#buscarsucursal" role="button" aria-expanded="false" aria-controls="buscarsucursal">
                                        Da click aqui
                                    </a>
                                </p>
                                <div class="collapse" id="buscarsucursal">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="sucursal">Sucursal</label>
                                            <select name="sucursal" id="sucursal" class="form-control">
                                                <option value="0">Seleccione sucursal</option>
                                                <?php
                                                foreach ($sucursales as $row) {
                                                    echo '<option value=' . $row['ID_SUCURSAL'] . '> ' . $row['NOMBRE_SUC'] . ' </option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-primary">Buscar</button>
                                    </div>
                                </div>
                            </form>
                            <!-- fin formulario -->

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                        <th>Descripcion</th>
                                        <th>Cantidad</th>
                                        <th>Precio</th>
                                        <th>Categoria</th>
                                        <th>Sucursal</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (is_array($productos) || is_object($productos)) {
                                        foreach ($productos as $row) {
                                            echo '<tr>';
                                            echo '<td>' . $row['ID_PRODUCTO'] . '</td>';
                                            echo '<td>' . $row['CODIGO'] . '</td>';
                                            echo '<td>' . $row['NOMBRE_PROD'] . '</td>';
                                            echo '<td>' . $row['DESC_PROD'] . '</td>';
                                            echo '<td>' . $row['CANTIDAD'] . '</td>';
                                            echo '<td>' . $row['PRECIO'] . '</td>';
                                            echo '<td>' . $row['NOMBRE_CAT'] . '</td>';
                                            echo '<td>' . $row['NOMBRE_SUC'] . '</td>';
                                            echo '<td>' . $row['NOMBRE_EST'] . '</td>';
                                            echo '<td><a class="btn btn-danger" href="?path=producto&accion=borrarProducto&cod=' . $row['CODIGO'] . '">Borrar</a></td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- /.content -->
</div>

<?php require_once "vista/inc/footer.php";  ?>