<?php require_once "vista/inc/header.php";  ?>

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-12">
                <div class="col-sm-12">
                    <h1>FORMULARIO PARA EDITAR PRODUCTO</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Detalles del producto</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-10">
                            <!-- formulario -->
                            <?php
                            $categorias = CategoriaCrud::buscarTodo();
                            $estados = EstadoCrud::buscarTodo();
                            $sucursales = SucursalCrud::buscarTodo();
                            ?>
                            <form class="form-horizontal" onsubmit="return validaEditarProducto()" method="POST" action="?path=producto&accion=actualizar&id=<?php echo $producto['ID_PRODUCTO'] ?>&suc=<?php echo $producto['ID_SUCURSAL'] ?>">
                                <div class="form-group row">
                                    <label for="codigo" class="col-sm-2 col-form-label">Codigo</label>
                                    <div class="col-sm-10">
                                        <?php
                                        if (isset($producto)) {
                                            echo '<input type="number" class="form-control" name="codigo" id="codigo" value="' . $producto['CODIGO'] . '">';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                                    <div class="col-sm-10">
                                        <?php
                                        if (isset($producto)) {
                                            echo '<input type="text" class="form-control" name="nombre" id="nombre" value="' . $producto['NOMBRE_PROD'] . '">';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="descripcion" class="col-sm-2 col-form-label">Descripcion</label>
                                    <div class="col-sm-10">
                                        <?php
                                        if (isset($producto)) {
                                            echo '<textarea class="form-control" name="descripcion" 
                                                    id="descripcion" rows="3">' . $producto['DESC_PROD'] . '</textarea>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="cantidad" class="col-sm-2 col-form-label">Cantidad</label>
                                    <div class="col-sm-10">
                                        <?php
                                        if (isset($producto)) {
                                            echo '<input type="number" class="form-control" name="cantidad" id="cantidad" value="' . $producto['CANTIDAD'] . '">';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="precio" class="col-sm-2 col-form-label">Precio</label>
                                    <div class="col-sm-10">
                                        <?php
                                        if (isset($producto)) {
                                            echo '<input type="number" class="form-control" name="precio" id="precio" value="' . $producto['PRECIO'] . '" >';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="idCategoria" class="col-sm-2 col-form-label">Categoria</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="idCategoria" id="idCategoria">
                                            <option value="0">Selecciones categoria ...</option>
                                            <?php
                                            foreach ($categorias as $row) {
                                                if (isset($producto) && $producto['ID_CATEGORIA'] == $row['ID_CATEGORIA']) {
                                                    echo '<option selected value=' . $row['ID_CATEGORIA'] . '> ' . $row['NOMBRE_CAT'] . ' </option>';
                                                } else {
                                                    echo '<option value=' . $row['ID_CATEGORIA'] . '> ' . $row['NOMBRE_CAT'] . ' </option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="idSucursal" class="col-sm-2 col-form-label">Sucursal</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="idSucursal" id="idSucursal">
                                            <option value="0">Selecciones sucursal ...</option>
                                            <?php
                                            foreach ($sucursales as $row) {
                                                if (isset($producto) && $producto['ID_SUCURSAL'] == $row['ID_SUCURSAL']) {
                                                    echo '<option selected value=' . $row['ID_SUCURSAL'] . '> ' . $row['NOMBRE_SUC'] . ' </option>';
                                                } else {
                                                    echo '<option value=' . $row['ID_SUCURSAL'] . '> ' . $row['NOMBRE_SUC'] . ' </option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="idEstado" class="col-sm-2 col-form-label">Estado</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="idEstado" id="idEstado">
                                            <option value="0">Selecciones estado ...</option>
                                            <?php
                                            foreach ($estados as $row) {
                                                if (isset($producto) && $producto['ID_ESTADO'] == $row['ID_ESTADO']) {
                                                    echo '<option selected value=' . $row['ID_ESTADO'] . '> ' . $row['NOMBRE_EST'] . ' </option>';
                                                } else {
                                                    echo '<option value=' . $row['ID_ESTADO'] . '> ' . $row['NOMBRE_EST'] . ' </option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-center h-100">
                                    <div class="col-sm-10 align-self-center text-center">
                                        <button type="submit" class="btn btn-success">Actualizar</button>
                                        <a href="?path=producto&accion=home" class="btn btn-danger">Cancelar</a>

                                    </div>
                                </div>
                            </form>
                            <!-- end formulario -->
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section> <!-- /.row -->
</div><!-- /.container-fluid -->

<!-- /.content -->
</div>

<?php require_once "vista/inc/footer.php";  ?>