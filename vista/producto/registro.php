<?php
require_once "vista/inc/header.php";
?>

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-12">
                <div class="col-sm-12">
                    <h1>FORMULARIO PARA AGREGAR PRODUCTO</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Detalles del producto</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-10">
                            <!-- formulario -->
                            <?php
                            $categorias = CategoriaCrud::buscarTodo();
                            $estados = EstadoCrud::buscarTodo();
                            ?>
                            <form class="form-horizontal" onsubmit="return validaCrearProducto()" method="POST" action="?path=producto&accion=nuevo">

                                <div class="form-group row">
                                    <label for="codigo" class="col-sm-2 col-form-label">Codigo</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="codigo" id="codigo">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="nombre" id="nombre">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="descripcion" class="col-sm-2 col-form-label">Descripcion</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="descripcion" id="descripcion" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="idCategoria" class="col-sm-2 col-form-label">Categoria</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="idCategoria" id="idCategoria">
                                            <option value="0">Seleccione ...</option>
                                            <?php
                                            foreach ($categorias as $row) {
                                                echo '<option value=' . $row['ID_CATEGORIA'] . '> ' . $row['NOMBRE_CAT'] . ' </option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="idEstado" class="col-sm-2 col-form-label">Estado</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="idEstado" id="idEstado">
                                            <option value="0">Seleccione ...</option>
                                            <?php
                                            foreach ($estados as $row) {
                                                echo '<option value=' . $row['ID_ESTADO'] . '> ' . $row['NOMBRE_EST'] . ' </option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-center h-100">
                                    <div class="col-sm-10 align-self-center text-center">
                                        <button type="submit" class="btn btn-success">Agregar</button>
                                        <a href="http://localhost/gestiostockphp/?path=producto&accion=home" class="btn btn-danger">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                            <!-- end formulario -->
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section> <!-- /.row -->
</div><!-- /.container-fluid -->

<!-- /.content -->
</div>

<?php require_once "vista/inc/footer.php";  ?>